---
title: "La nouvelle arme du bloc: la Split"
date: "2022-06-29"
categories:
  - "Actualités"
tags:
  - "matos"
  - "compétition"

thumbnail: "img/la_split.png"
lead: ""
comments: false
authorbox: false
pager: true
toc: false
sidebar: "right"
widgets:
  - "search"
  - "recent"
  - "taglist"
---

On connaissait la Magn utilisée (abusée) par tous les grimpeurs du monde, la Pof réservée aux puristes de Fontainebleau... place maintenant à la Split !

Mais alors quelle est cette nouvelle bouteille que l'on peut dès à présent retrouver dans les rayons du vieux campeur ?

La Split est la réponse de Petzl à une problématique du bloc moderne: la multiplication des prises dites "bi-texturées", voir même exclusivement en plastique.

En effet, la magnésie se révèle inutile sur ces prises qui ne possèdent pas de grain. Qui n'a jamais pesté lors d'une séance de bloc contre ces nouvelles prises impossibles à tenir ?

Nous avons demandé à [trouver un nom], designer produit chez Petzl, de nous parler de ce nouveau produit:

> L'idée nous ait venue après l'étape de coupe de monde d'Innsbruck. En voyant Jakob Schubert cracher dans ses main pour tenter de gagner de l'adhérence, nous avons réalisé qu'il existait un véritable besoin.

Petzl décide alors d'investir ce nouveau marché, en développant une solution complémentaire à la classique poudre de Magnésium.

## Une formule secrète

Mais alors de quoi est composée cette fameuse Split ? [trouver un nom] nous donne un début de réponse, sans pour autant nous dévoiler tous les secrets de la formule gardée précieusement par Petzl.

> À l'origine du produit, il y a un liquide biologique très simple: la salive. La question est ensuite de savoir à partir de quel animal la produire, afin d'obtenir une consistance et une adhérence optimales. Tout le bénéfice revient à nos équipes de recherche qui ont fait un travail fantastique pour tester et comparer des dizaines de salives, afin de proposer la formule optimale qui saura satisfaire les compétiteurs les plus exigents.

## Une gamme qui s'étoffe déjà

Mais quid du grimpeur amateur qui souhaite lui-aussi profiter de cette nouvelle solution ? Il ne sera pas oublié. En effet, Petzl prévoit d'étoffer très rapidement sa gamme afin de proposer des solutions adaptées à tous les budgets.

Au modèle "Split Pro" disponible actuellement en magasin devrait bientôt s'ajouter le "Split Slug", une version plus abordable.

Mais l'ambition du constructeur ne s'arrête pas là, et les idées ne semblent pas manquer pour élargir la gamme:

> Nous travaillons actuellement avec nos athlètes sponsorisés afin de proposer des éditions spéciales de la Split, à base de vraie salive de grimpeurs. Grâce à notre partenariat avec une équipe de recherche de l'université de Munich, nous analysons la salive de nos athlètes afin de détecter des sucs particulirement efficaces et de les proposer en édition limitée.

Alors à quand le tube collector de "Split d'Ondra" ? Peut-être plus tôt qu'on ne le pense, celui-ci ayant apparemment obtenu la note maximale aux tests réalisés par Petzl.

## Top ou flop ?

Cette magnésie 2.0 saura-t-elle convaincre les consommateurs ? En tout cas les compétiteur eux sont déjà convaincus. Nous avons profité de l'étape de difficulté d'Innsbruck pour demander son avis à Chaehyun Seo, La jeune révélation coréenne de la saison dernière qui continue à impressionner cette année avec de très bons résultats en bloc:

> Split은 오늘날의 경쟁 스타일에서 실제 투쟁에 대한 솔루션입니다. 이미 3리터를 구입했고 Adam Ondra 스페셜 에디션을 기대하고 있습니다.

Nul doute que cette solution devrait trouver rapidement sa place dans nos coeurs de grimpeurs.