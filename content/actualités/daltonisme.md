---
title: "Couleurs: quand les ouvreurs se lâchent"
date: "2022-07-24"
categories:
  - "Actualités"
tags:
  - "edito"
  - "compétition"

thumbnail: "img/daltonisme.jpg"
lead: ""
comments: false
authorbox: false
pager: true
toc: false
sidebar: "right"
widgets:
  - "search"
  - "recent"
  - "taglist"
---

À l'approche de la fin de la saison de bloc, un constat s'impose: les ouvreurs semblent cette année avoir été recrutés sur la base de leur niveau de daltonisme. Du daltonisme léger lors de la première finale de Salt Lake au daltonisme sévère pour les finales de Meiringen, le public aura pu admirer cette année tout l'éventail des associations de couleurs de merde.

10 avril, Meiringen. C'est la première étape de la saison, il s'agit de frapper un grand coup. Une équipe entière d'ouvreurs de niveau mondial entame alors une réflexion poussée à propos du M4. Peut-être qu'en associant du jaune poussin et du vert courgette, le public ne remarquera pas qu'ils ont ouvert un pan Güllich ?

![](/img/M4.png)

_admirez ce choix esthétique digne des plus grands artistes de notre temps_

Seoul, le mot d'ordre est à la sobriété. Les blocs bleus et noirs affluent, la folie des couleurs ayant semble-t-il abandonnée les ouvreurs. Une erreur stratégique car, en pleine possession de leur acuité visuelle, les spectateurs ont eu tout loisir de remarquer l'objectif principal de ces finales: casser un maximum de coudes et de nuques, mais surtout laisser la possibilité aux hommes de raccourcir certains blocs de deux ou trois mouvements. 

![](/img/skip.png)

_que se passe-t-il quand vous mettez un énorme bac au milieu d'un bloc ? Sans surprise, il se fait raccourcir de 5 mouvements..._

Autre innovation notable en finale: l'importation du phénomène de résurgence, que l'on ne connaissait jusque là qu'en falaise. Après l'arrivée des coincements de mains, cette nouvelle innovation devrait ravire les amateurs d'expéditions foireuses. Comme nous le rappel le grand Sean Villanueva (qu'il n'est plus exclu de retrouver sur le circuit international au vu de ces nouveautés):

> Quand c'est du rocher pourri, ou quand c'est dégueulasse, quand c'est mouillé c'est un autre défi. Le défi est le même. C'est un autre défi mais le défi est le même. 

Sean Villanueva, _Relais Vertical Ep.42_

 Point d'orgue de cette nouvelle idée, le concept de "prises piscine" sera inventé dans la foulée, lorsque les bacs remplis d'eau nécessiteront une équipe d'écopeurs en plus des classiques brosseurs.

Première finale de Salt Lake City, le W1 est un bonbon. Un choix presque aussi machiste que les [fabuleux cameramans de l'IFSC de la saison dernière](https://www.20min.ch/fr/story/images-sexistes-scandale-aux-championnats-du-monde-248931142367). Une semaine plus tard, les prises sont les mêmes, et le bloc rose Barbie de nouveau pour les filles. Les hommes grimperont dans les blocs bleus et noirs, parce que ça c'est des vraies couleurs de mec. La Licra notera également la prédominance dans le M3 des prises blanches sur les noires, à l'image de la diversité dans le haut niveau.

À Brixen rien à dire, les ouvreurs ont la notion du style et les blocs sont classes. Chez les femmes c’est le jaune qui est à l’honneur, mais aucune des trois japonaises ne fera de podium. 

Cela nous amène finalement à Innsbruck, épreuve qui signe le grand retour des expériences colorimétriques. 

![](/img/W1.png)

_comment ne pas admirer le rendu esthétique de ces prises bicolores ?_

Pour tenter de comprendre le choix ambitieux du W1 nous avons interviewé Merlin K., double champion du monde de daltonisme et grimpeur correct. Il décortique pour nous la finesse des techniques déployées par les ouvreurs:

> On peut tout d'abord constater que les ouvreurs ont fait un formidable travail esthétique : l’association des gris #70726E et #C4C9C7 des prises avec le gris #677179 du mur donne lieu à un éclatant spectacle. Au-delà de l'aspect visuel, ce contraste est surtout crucial pour la réalisation du bloc : les prises ne faisant chacune qu’une quarantaine de centimètres de diamètre, il fallait pouvoir les distinguer facilement sur le mur.

Et pour ce qui est du niveau technique et de la méthode ?

> Il est évident qu'il faut jeter.

Une analyse dont la pertinence n'aura eu d'égal que la difficulté des ouvertures de l'étape de difficulté de Chamonix. Mais ce point fera très sûrement l'objet d'un prochain article.