---
title: "Les TC pros"
date: "2022-06-29"
categories:
  - "Tests Matos"
tags:
  - "matos" 

thumbnail: "img/TC_pro.jpg"
lead: ""
comments: false
authorbox: false
pager: true
toc: false
sidebar: "right"
widgets:
  - "search"
  - "recent"
  - "taglist"
---

Chausson iconique de La Sportiva, les TC pros passent aujourd'hui sur le banc de test de Escalader. Alors que penser de ces chaussons, créés pour être l'arme ultime du bloc en compétition ?

Évidemment, il y aura toujours des vieux cons pour râler. Mais qu'on le veuille ou non, le bloc moderne est aujourd'hui un élément central de la pratique de l'escalade dans le monde. Sport idéal du citadin, celui-ci se développe de manière exponentielle depuis quelques années et bien malin qui pourra dire où cela s'arrêtera.

C'est fort de ce constat que La Sportiva s'est décidée à élargir sa gamme de chaussons. En effet sur ce marché des chaussons de bloc moderne, la référence est depuis longtemps l'incontournable Drago de Scarpa.

Mais tout va peut-être changer avec cette nouvelle arme de précision de La Sportiva. Grace à des contacts exclusifs à l'usine La Sportiva de Bourg-perdu-en-Creuse, nous en avons pu tester en avant première ce chausson. Nous vous faisons aujourd'hui nos premiers retours.

# Des performances imbattables

Nul doute que nous avons bien en fasse de nous un chausson destiné au bloc moderne. La semelle est une des moins souples du marché, ce qui promet à coup sûr des performances inégalées pour le bloc en coincement que les ouvreurs auront dissimulé (les coquins) au milieu des 50 jetés. Cette rigidité sera également bienvenue pour les courses d'élan des Run&Jump, d'autant plus avec cette année deux étapes à Salt Lake City où les tapis tiennent plus du canapé de mamie que du tapis de gym.

Mais ce chausson a bien plus à offrir. Avec un choix ambitieux de serrage à lacets, La Sportiva se place en opposition des classiques du genre. Ce choix s'avère payant. Finit le temps perdu à changer de chaussons au milieu du bloc, alors qu'il suffisait de grimper mieux. Avec les lacets ce n'est tout simplement plus envisageable. La réduction de la charge mentale est conséquente, avec un effet immédiat sur les chances de réussite.

Dernier point mais non des moindres, le chausson remonte haut pour couvrir complètement la cheville. Surprenante au premier abord, cette idée s'avère payante. En immobilisant la cheville du grimpeur, il devient possible d'ignorer une éventuelle entorse afin de finir sa compétition le plus tranquillement du monde après une mauvaise chute dans ce quintuple jeté 360° salto arrière double vrille qui n'était décidément pas facile. Une solution inspirée de la performance de Mia Krampl il y a deux ans. Blessée, celle-ci avait sorti sa meilleure performance en finissant 2ème. Avec ces chaussons et en se basant sur cette expérience, La Sportiva ouvre la porte à des blessures volontaires permettant de décupler les performances des athlètes.

# Un chausson réservé à un usage bien précis

Alors les TC Pros sont ils vos prochains chaussons ? Tout dépend de votre usage. Si comme on l'a vu ils semblent être l'arme ultime du bloc de compétition, il n'en est pas de même pour les autres disciplines qui composent notre sport.

Ces chaussons s'avèrent particulièrement inefficaces en falaise, et plus particulièrement en grande voie. La protection des chevilles que nous avions tant appréciée pose problème, en venant réveiller les piqûres de moustique du bivouac de la veille, provoquant irritations et mauvaise humeur.

De même, les lacets deviennent vite votre pire ennemi. Quoi de pire en effet pour enlever ses chaussons au relais, ce qui est quand même la plus grande source de plaisir et la raison pour laquelle on pratique cette activité ?

Enfin, la rigidité si utile pour prendre son élan sur des tapis de bloc est ici parfaitement inutile. Elle devient même dangereuse, avec la possibilité de se bloquer les pieds dans des fissures, ce qui en plus d'être parfaitement inutile doit sûrement faire très mal.

# Conclusion

Notre conclusion sera brève, car sans appel: ces chaussons sont un coup de cœur ! Seul bémol, leur prix (aux alentours de 150€) qui en font tout simplement les chaussons les plus cher du marché. Pour le reste, rien à leur reprocher. 

Avec ce modèle, le positionnement de La Sportiva est clair et assumé. Concurrencer directement les Dragos, en proposant un chausson ultra spécialisé qui saura convaincre les grimpeurs et grimpeuses de bloc les plus acharnés. Pour les autres, surtout les montagnards, passez votre chemin ! Ces chaussons ne seront pas pour vous.